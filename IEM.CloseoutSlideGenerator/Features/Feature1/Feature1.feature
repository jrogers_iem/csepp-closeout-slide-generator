﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="a31660d0-8faf-4a02-9fe8-098a46064e88" description="Activate this feature to enable the Content Types, Lists, Web Parts and Navigation elements in this web." featureId="a31660d0-8faf-4a02-9fe8-098a46064e88" imageUrl="" solutionId="00000000-0000-0000-0000-000000000000" title="IEM Closeout Slide Generator" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="2cfd767c-7143-4ee1-86ef-c93b15755e43" />
    <projectItemReference itemId="393e06d4-2ba4-45fa-b3e1-53a0a695e59e" />
    <projectItemReference itemId="f10669b3-c6e6-4ab9-9753-b5b713671504" />
    <projectItemReference itemId="e8943734-4c72-4b7b-b75e-e29d92823a95" />
    <projectItemReference itemId="e29ae2d8-a5f4-4a5e-a6b2-65341feee9ae" />
    <projectItemReference itemId="839621ee-fe5f-49e9-b9ac-788cf7a7e7ed" />
    <projectItemReference itemId="6a063135-2b63-40af-941a-3988a175d0c6" />
    <projectItemReference itemId="e38f4f13-9f8d-4a6e-8b95-ee1653ce362f" />
    <projectItemReference itemId="953955b3-3f1a-4666-8975-03f9b2cbf2f5" />
    <projectItemReference itemId="cd2eb79a-3fb0-470b-b104-8f5de7a33c15" />
    <projectItemReference itemId="e1257986-c75d-4f0a-b428-a80d23e54d02" />
  </projectItems>
</feature>