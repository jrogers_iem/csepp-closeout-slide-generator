﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChartWebPartUserControl.ascx.cs" Inherits="IEM.CloseoutSlideGenerator.WebParts.ChartWebPart.ChartWebPartUserControl" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>





<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="true"></asp:GridView>
<table style="width: 100%;">
    <tr>
        <td>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" >
                        <asp:ListItem Text="Bar" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Pie" ></asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                    <asp:Chart ID="Chart1" runat="server">
                        <Legends>
                            <asp:Legend 
                                Name="Legend1"
                                BackColor="AliceBlue"
                                ForeColor="CadetBlue"
                                BorderColor="LightBlue"
                                Docking="Right"
                                >
                            </asp:Legend>
                        </Legends>

                        <series>
                            <asp:Series ChartType="Bar" Name="Series1">
            
                            </asp:Series>
                        </series>
                        <chartareas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </chartareas>
                    </asp:Chart>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    Busy....
                </ProgressTemplate>
            </asp:UpdateProgress>
            
        </td>
        <td>
            &nbsp;
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:RadioButtonList ID="RadioButtonList2" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged" >
                        <asp:ListItem Text="Bar" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Pie" ></asp:ListItem>
                    </asp:RadioButtonList>      
            <asp:Chart ID="Chart2" runat="server">
                <Legends>
                    <asp:Legend 
                        Name="Legend1"
                        BackColor="AliceBlue"
                        ForeColor="CadetBlue"
                        BorderColor="LightBlue"
                        Docking="Right"
                    >
                    </asp:Legend>
                </Legends>
            <series>
                <asp:Series ChartType="Bar" Name="Series1">
            
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        </asp:Chart>
                    </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                <ProgressTemplate>
                    Busy....
                </ProgressTemplate>
            </asp:UpdateProgress>
        </td>
        <td>
            
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Chart ID="Chart4" runat="server">
            <series>
                <asp:Series XValueType="Double" IsValueShownAsLabel="true" LegendText="Billable hours" ChartType="RangeColumn" Color="LightGreen" 
                    Name="Completed" BorderColor="180, 26, 59, 105" CustomProperties="DrawingStyle=Cylinder" ShadowColor="DarkGreen"></asp:Series>
                <asp:Series XValueType="Double" IsValueShownAsLabel="true" Legend="non Billable hours" ChartType="RangeColumn" Color="LightYellow" 
                    Name="In Progress" BorderColor="180, 26, 59, 105" CustomProperties="DrawingStyle=Cylinder" ShadowColor="Transparent"></asp:Series>
                <asp:Series XValueType="Double" IsValueShownAsLabel="true" Legend="non Billable hours" ChartType="RangeColumn" Color="LightCoral" 
                    Name="Not Started" BorderColor="180, 26, 59, 105" CustomProperties="DrawingStyle=Cylinder" ShadowColor="Transparent"></asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        </asp:Chart>
        </td>             
        <td>
            <asp:Chart ID="Chart3" runat="server">
            <series>
                <asp:Series XValueType="Double" IsValueShownAsLabel="true" LegendText="Billable hours" ChartType="RangeColumn" Color="LightGreen" 
                    Name="Green" BorderColor="180, 26, 59, 105" CustomProperties="DrawingStyle=Cylinder" ShadowColor="DarkGreen"></asp:Series>
                <asp:Series XValueType="Double" IsValueShownAsLabel="true" Legend="non Billable hours" ChartType="RangeColumn" Color="LightYellow" 
                    Name="Amber" BorderColor="180, 26, 59, 105" CustomProperties="DrawingStyle=Cylinder" ShadowColor="Transparent"></asp:Series>
                <asp:Series XValueType="Double" IsValueShownAsLabel="true" Legend="non Billable hours" ChartType="RangeColumn" Color="LightCoral" 
                    Name="Red" BorderColor="180, 26, 59, 105" CustomProperties="DrawingStyle=Cylinder" ShadowColor="Transparent"></asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        </asp:Chart>
        </td>        
    </tr>
</table>
