﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Linq;
using System.Data;
using System.Drawing;
using Microsoft.SharePoint;

using System.Web.UI.DataVisualization.Charting;

namespace IEM.CloseoutSlideGenerator.WebParts.ChartWebPart
{
    public partial class ChartWebPartUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                        

            if (!Page.IsPostBack)
            {
                DataTable T1 = GetData1();
                BindChart1(T1, System.Web.UI.DataVisualization.Charting.SeriesChartType.Bar);

                DataTable T2 = GetData2();
                BindChart2(T2, System.Web.UI.DataVisualization.Charting.SeriesChartType.Bar);
                

                DataTable T3 = GetData3();
                BindChart3(T3);

                DataTable T4 = GetData4();
                BindChart4(T4);
            }
        }

        protected DataTable GetItems()
        {
            string Query = @"<Where><IsNotNull><FieldRef Name=""ID"" /></IsNotNull></Where>";
            string ViewFields = @"<FieldRef Name=""ID"" /><FieldRef Name=""Title"" /><FieldRef Name=""ContentType"" /><FieldRef Name=""AssignedTo"" /><FieldRef Name=""Jurisdiction"" /><FieldRef Name=""Status"" /><FieldRef Name=""Rating"" /><FieldRef Name=""PlanType"" /><FieldRef Name=""PercentComplete"" />";
            string ViewAttrs = @"Scope=""Recursive""";
            uint RowLimit = 0;

            var oQuery = new SPQuery();
            oQuery.Query = Query;
            oQuery.ViewFields = ViewFields;
            oQuery.ViewAttributes = ViewAttrs;
            oQuery.RowLimit = RowLimit;

            SPList List = SPContext.Current.Web.Lists["Closeout Tasks"];

            SPListItemCollection Items = List.GetItems(oQuery);

            return Items.GetDataTable();
        }

        public DataTable GetData1()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("Task Status", typeof(string));
            dtReport.Columns.Add("Count", typeof(int));
                       

            DataTable T = GetItems();

            
            DataRow[] N = T.Select("[Status] = 'Not Started'");
            DataRow[] I = T.Select("[Status] = 'In Progress'");
            DataRow[] C = T.Select("[Status] = 'Completed'");
            dtReport.Rows.Add("Not Started", N.Length);
            dtReport.Rows.Add("In Progress", I.Length);
            dtReport.Rows.Add("Completed", C.Length);

            return dtReport;
        }

        public DataTable GetData2()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("Rating", typeof(string));
            dtReport.Columns.Add("Count", typeof(int));
            

            DataTable T = GetItems();

            //return T;
            DataRow[] G = T.Select("[Rating] = 'Green'");
            DataRow[] A = T.Select("[Rating] = 'Amber'");
            DataRow[] R = T.Select("[Rating] = 'Red'");
            dtReport.Rows.Add("Green", G.Length);
            dtReport.Rows.Add("Amber", A.Length);
            dtReport.Rows.Add("Red", R.Length);

            return dtReport;
        }

        public DataTable GetData3()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("Jurisdiction", typeof(string));
            //dtReport.Columns.Add("Count", typeof(int));
            dtReport.Columns.Add("Green", typeof(int));
            dtReport.Columns.Add("Amber", typeof(int));
            dtReport.Columns.Add("Red", typeof(int));
            

            DataTable T = GetItems();

            DataView view = new DataView(T);
            DataTable distinctValues = view.ToTable(true, "Jurisdiction");

            //return T;
            DataRow[] J = distinctValues.Select();

            foreach (DataRow Row in J)
            {
                string Jurisdiction = Row["Jurisdiction"].ToString();

                DataRow[] G = T.Select("[Jurisdiction] = '"+Jurisdiction+"' AND [Rating] = 'Green'");
                DataRow[] A = T.Select("[Jurisdiction] = '"+Jurisdiction+"' AND [Rating] = 'Amber'");
                DataRow[] R = T.Select("[Jurisdiction] = '"+Jurisdiction+"' AND [Rating] = 'Red'");
                //dtReport.Rows.Add("Green", G.Length);
                //dtReport.Rows.Add("Amber", A.Length);
                //dtReport.Rows.Add("Red", R.Length);

                DataRow Ro = dtReport.NewRow();

                Ro["Jurisdiction"] = Jurisdiction;
                Ro["Green"] = G.Length;
                Ro["Amber"] = A.Length;
                Ro["Red"] = R.Length;

                dtReport.Rows.Add(Ro);
            }
            return dtReport;
        }

        public DataTable GetData4()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("Jurisdiction", typeof(string));
            //dtReport.Columns.Add("Count", typeof(int));
            dtReport.Columns.Add("Completed", typeof(int));
            dtReport.Columns.Add("In Progress", typeof(int));
            dtReport.Columns.Add("Not Started", typeof(int));
            

            DataTable T = GetItems();

            DataView view = new DataView(T);
            DataTable distinctValues = view.ToTable(true, "Jurisdiction");

            //return T;
            DataRow[] J = distinctValues.Select();

            foreach (DataRow Row in J)
            {
                string Jurisdiction = Row["Jurisdiction"].ToString();

                DataRow[] G = T.Select("[Jurisdiction] = '" + Jurisdiction + "' AND [Status] = 'Completed'");
                DataRow[] A = T.Select("[Jurisdiction] = '" + Jurisdiction + "' AND [Status] = 'In Progress'");
                DataRow[] R = T.Select("[Jurisdiction] = '" + Jurisdiction + "' AND [Status] = 'Not Started'");
                

                DataRow Ro = dtReport.NewRow();

                Ro["Jurisdiction"] = Jurisdiction;
                Ro["Completed"] = G.Length;
                Ro["In Progress"] = A.Length;
                Ro["Not Started"] = R.Length;

                dtReport.Rows.Add(Ro);
            }
            return dtReport;
        }
        public void BindChart1(DataTable dtReport, System.Web.UI.DataVisualization.Charting.SeriesChartType CT)
        {
            string[] x = new string[dtReport.Rows.Count];
            double[] y = new double[dtReport.Rows.Count];
            for (int i = 0; i < dtReport.Rows.Count; i++)
            {
                x[i] = dtReport.Rows[i][0].ToString();
                y[i] = Convert.ToDouble(dtReport.Rows[i][1]);
            }
            Chart1.Series[0].Points.DataBindXY(x, y);

            Chart1.Series[0].ChartType = CT;
            if (CT == SeriesChartType.Bar)
            {
                //Chart1.Legends[0]
            }

            Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -50;
            Chart1.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Verdana", 8, System.Drawing.FontStyle.Bold);
            Chart1.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Verdana", 8, System.Drawing.FontStyle.Bold);
            Chart1.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            Chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            Chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = true;
            Chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = ColorTranslator.FromHtml("#e5e5e5");
            Chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.Enabled = true;
            Chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = ColorTranslator.FromHtml("#e5e5e5");
            Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Bold);
            Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Bold);
            Chart1.Series[0].IsValueShownAsLabel = true;
            Chart1.ChartAreas["ChartArea1"].AxisY.Title = "Count";
            Chart1.ChartAreas["ChartArea1"].AxisX.Title = "Task Status";
            Chart1.Width = 500;
        }

        public void BindChart2(DataTable dtReport, System.Web.UI.DataVisualization.Charting.SeriesChartType CT)
        {
            string[] x = new string[dtReport.Rows.Count];
            double[] y = new double[dtReport.Rows.Count];
            for (int i = 0; i < dtReport.Rows.Count; i++)
            {
                x[i] = dtReport.Rows[i][0].ToString();
                y[i] = Convert.ToDouble(dtReport.Rows[i][1]);
            }
            Chart2.Series[0].Points.DataBindXY(x, y);

            Chart2.Series[0].ChartType = CT;

            Chart2.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -50;
            Chart2.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Verdana", 8, System.Drawing.FontStyle.Bold);
            Chart2.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Verdana", 8, System.Drawing.FontStyle.Bold);
            Chart2.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            Chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            Chart2.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = true;
            Chart2.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = ColorTranslator.FromHtml("#e5e5e5");
            Chart2.ChartAreas["ChartArea1"].AxisY.MajorGrid.Enabled = true;
            Chart2.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = ColorTranslator.FromHtml("#e5e5e5");
            Chart2.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Bold);
            Chart2.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Bold);
            Chart2.Series[0].IsValueShownAsLabel = true;
            Chart2.ChartAreas["ChartArea1"].AxisY.Title = "Count";
            Chart2.ChartAreas["ChartArea1"].AxisX.Title = "Rating";
            Chart2.Width = 500;
        }

        public void BindChart3(DataTable dtReport)
        {

            //Get the DISTINCT Countries.
            for (int i = 0; i < dtReport.Rows.Count; i++)
            {
                double G = double.Parse(dtReport.Rows[i]["Green"].ToString());
                double A = double.Parse(dtReport.Rows[i]["Amber"].ToString());
                double R = double.Parse(dtReport.Rows[i]["Red"].ToString());

                //if(G >= 1)
                    Chart3.Series["Green"].Points.AddY(double.Parse(dtReport.Rows[i]["Green"].ToString()));

                //if(A >= 1)
                    Chart3.Series["Amber"].Points.AddY(double.Parse(dtReport.Rows[i]["Amber"].ToString()));

                //if(R >= 1)
                    Chart3.Series["Red"].Points.AddY(double.Parse(dtReport.Rows[i]["Red"].ToString()));
                //Chart3.Series["nonBillable"].Points.AddY(2);
                //Chart3.Series["Billable"].Points.AddY(1);
                //Chart3.Series["nonBillable"].Points.AddY(4);


                Chart3.Series["Green"].Points[i].AxisLabel = dtReport.Rows[i]["Jurisdiction"].ToString();
                //Chart3.Series["Billable"].Points[1].AxisLabel = "Richard";
                //Chart3.Series["Billable"].Points[2].AxisLabel = "Harry";
            }

            

            Chart3.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -50;
            Chart3.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Verdana", 8, System.Drawing.FontStyle.Bold);
            Chart3.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Verdana", 8, System.Drawing.FontStyle.Bold);
            Chart3.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            Chart3.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            Chart3.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = true;
            //Chart2.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = ColorTranslator.FromHtml("#e5e5e5");
            Chart3.ChartAreas["ChartArea1"].AxisY.MajorGrid.Enabled = true;
            //Chart2.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = ColorTranslator.FromHtml("#e5e5e5");
            Chart3.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Bold);
            Chart3.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Bold);
            Chart3.Series[0].IsValueShownAsLabel = true;
            Chart3.ChartAreas["ChartArea1"].AxisY.Title = "Rating Count";
            Chart3.ChartAreas["ChartArea1"].AxisX.Title = "Jurisdiction";
            Chart3.Width = 500;
            Chart3.Height = 500;
        }

        public void BindChart4(DataTable dtReport)
        {

            //Get the DISTINCT Countries.
            for (int i = 0; i < dtReport.Rows.Count; i++)
            {
                double G = double.Parse(dtReport.Rows[i]["Completed"].ToString());
                double A = double.Parse(dtReport.Rows[i]["In Progress"].ToString());
                double R = double.Parse(dtReport.Rows[i]["Not Started"].ToString());

                //if(G >= 1)
                Chart4.Series["Completed"].Points.AddY(double.Parse(dtReport.Rows[i]["Completed"].ToString()));

                //if(A >= 1)
                Chart4.Series["In Progress"].Points.AddY(double.Parse(dtReport.Rows[i]["In Progress"].ToString()));

                //if(R >= 1)
                Chart4.Series["Not Started"].Points.AddY(double.Parse(dtReport.Rows[i]["Not Started"].ToString()));
                //Chart3.Series["nonBillable"].Points.AddY(2);
                //Chart3.Series["Billable"].Points.AddY(1);
                //Chart3.Series["nonBillable"].Points.AddY(4);


                Chart4.Series["Completed"].Points[i].AxisLabel = dtReport.Rows[i]["Jurisdiction"].ToString();
                //Chart3.Series["Billable"].Points[1].AxisLabel = "Richard";
                //Chart3.Series["Billable"].Points[2].AxisLabel = "Harry";
            }

            
            Chart4.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -50;
            Chart4.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Verdana", 8, System.Drawing.FontStyle.Bold);
            Chart4.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Verdana", 8, System.Drawing.FontStyle.Bold);
            Chart4.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            Chart4.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            Chart4.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = true;
            //Chart2.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = ColorTranslator.FromHtml("#e5e5e5");
            Chart4.ChartAreas["ChartArea1"].AxisY.MajorGrid.Enabled = true;
            //Chart2.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = ColorTranslator.FromHtml("#e5e5e5");
            Chart4.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Bold);
            Chart4.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Bold);
            Chart4.Series[0].IsValueShownAsLabel = true;
            Chart4.ChartAreas["ChartArea1"].AxisY.Title = "Task Status Count";
            Chart4.ChartAreas["ChartArea1"].AxisX.Title = "Jurisdiction";
            Chart4.Width = 500;
            Chart4.Height = 500;
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {            

            DataTable T1 = GetData1();

            if (RadioButtonList1.SelectedIndex == 0)
            {
                BindChart1(T1, System.Web.UI.DataVisualization.Charting.SeriesChartType.Bar);
            }
            else
            {
                BindChart1(T1, System.Web.UI.DataVisualization.Charting.SeriesChartType.Pie);
            }

            DataTable T2 = GetData2();

            if (RadioButtonList2.SelectedIndex == 0)
            {
                BindChart2(T2, System.Web.UI.DataVisualization.Charting.SeriesChartType.Bar);
            }
            else
            {
                BindChart2(T2, System.Web.UI.DataVisualization.Charting.SeriesChartType.Pie);
            }
        }

        protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable T1 = GetData1();

            if (RadioButtonList1.SelectedIndex == 0)
            {
                BindChart1(T1, System.Web.UI.DataVisualization.Charting.SeriesChartType.Bar);
            }
            else
            {
                BindChart1(T1, System.Web.UI.DataVisualization.Charting.SeriesChartType.Pie);
            }

            DataTable T2 = GetData2();

            if (RadioButtonList2.SelectedIndex == 0)
            {
                BindChart2(T2, System.Web.UI.DataVisualization.Charting.SeriesChartType.Bar);
            }
            else
            {
                BindChart2(T2, System.Web.UI.DataVisualization.Charting.SeriesChartType.Pie);
            }
        }
    }
}
