﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using System.IO;
using Spire.Presentation;
using Microsoft.SharePoint;

namespace IEM.CloseoutSlideGenerator.Util
{
    internal sealed class SlideUtil : BaseUtil
    {
        #region Create Slides
        public void CreateSlides(int[] IDs, String LibraryName)
        {
            try
            {                

                //Get the list items...
                SPSite Site = SPContext.Current.Site;
                
                    using (SPWeb Web = Site.OpenWeb())
                    {
                        SPList List = Web.Lists["Closeout Tasks"];

                        SPListItem Item1 = List.GetItemByIdAllFields(IDs[0]);
                        SPListItem Item2 = List.GetItemByIdAllFields(IDs[1]);
                        SPListItem Item3 = List.GetItemByIdAllFields(IDs[2]);
                        SPListItem Item4 = List.GetItemByIdAllFields(IDs[3]);
                        SPListItem Item5 = List.GetItemByIdAllFields(IDs[4]);
                        SPListItem Item6 = List.GetItemByIdAllFields(IDs[5]);
                        //SPListItem Item7 = List.GetItemByIdAllFields(IDs[6]);

                        SPListItem[] Items = { Item1, Item2, Item3, Item4, Item5, Item6 };

                        String Jurisdiction = Item1["Jurisdiction"].ToString();

                        //SPList Library = Web.Lists["Closeout Slides"];

                        int AssistanceFileId = CreateDocument("Closeout Assistance - " + Jurisdiction + ".pptx", "Closeout Assistance Slide", "Closeout Slides");
                        int PlanningFileId = CreateDocument("Closeout Planning - " + Jurisdiction + ".pptx", "Closeout Planning Slide", "Closeout Slides");

                        SPList SlideLibrary = Web.Lists[LibraryName];

                        SPFile AssistanceFile = SlideLibrary.GetItemById(AssistanceFileId).File;
                        SPFile PlanningFile = SlideLibrary.GetItemById(PlanningFileId).File;

                        //Console.WriteLine("Got files...");
                        System.Threading.Thread.Sleep(300);

                        System.Collections.ArrayList PlanningList = new System.Collections.ArrayList();

                        for (int i = 0; i < Items.Length; i++)
                        {
                            if (Items[i].ContentType.Name.Contains("Planning"))
                            {
                                PlanningList.Add(Items[i]);
                            }

                            if (Items[i].ContentType.Name.Contains("Assistance"))
                            {
                                AssistanceFile.Item["Jurisdiction"] = Jurisdiction;//Items[i].GetFormattedValue("Jurisdiction");
                                AssistanceFile.Item["Rating"] = Items[i].GetFormattedValue("Rating");
                                AssistanceFile.Item.SystemUpdate(false);

                                System.Threading.Thread.Sleep(300);

                                Presentation PPTX = new Presentation(AssistanceFile.OpenBinaryStream(), Spire.Presentation.FileFormat.Pptx2013);

                                ITable table = null;

                                foreach (IShape shape in PPTX.Slides[0].Shapes)
                                {
                                    if (shape is ITable)
                                    {
                                        table = (ITable)shape;
                                        table[0, 0].TextFrame.Text = Items[i].GetFormattedValue("Issue1");
                                        table[0, 1].TextFrame.Text = Items[i].GetFormattedValue("Issue2");
                                        table[0, 2].TextFrame.Text = Items[i].GetFormattedValue("Issue3");
                                    }
                                }
                                System.Threading.Thread.Sleep(300);
                                MemoryStream MS = new MemoryStream();

                                PPTX.SaveToFile(MS, Spire.Presentation.FileFormat.Pptx2013);

                                List.RootFolder.Files.Add(AssistanceFile.ServerRelativeUrl, MS.ToArray(), true);
                            }
                        }
                        System.Threading.Thread.Sleep(300);

                        PlanningFile.Item["Jurisdiction"] = Jurisdiction;
                        String Rating = GetRating(PlanningList);
                        PlanningFile.Item["Rating"] = Rating;
                        PlanningFile.Item.SystemUpdate(false);



                        Presentation PPTX0 = new Presentation(PlanningFile.OpenBinaryStream(), Spire.Presentation.FileFormat.Pptx2013);

                        ITable table0 = null;

                        foreach (IShape shape in PPTX0.Slides[0].Shapes)
                        {
                            if (shape is ITable)
                            {
                                table0 = (ITable)shape;

                                for (int i = 0; i < PlanningList.Count; i++)
                                {
                                    SPListItem Item = (SPListItem)PlanningList[i];

                                    String PlanType = Item.GetFormattedValue("PlanType");

                                    String[] Types = { "People", "Property", "Plans", "Purse", "Systems" };

                                    switch (PlanType)
                                    {
                                        case "People":
                                            table0[1, 1].TextFrame.Text = Items[i].GetFormattedValue("PresentState");
                                            table0[2, 1].TextFrame.Text = Items[i].GetFormattedValue("TransitionState");
                                            table0[3, 1].TextFrame.Text = Items[i].GetFormattedValue("FutureState");
                                            break;
                                        case "Property":
                                            table0[1, 2].TextFrame.Text = Items[i].GetFormattedValue("PresentState");
                                            table0[2, 2].TextFrame.Text = Items[i].GetFormattedValue("TransitionState");
                                            table0[3, 2].TextFrame.Text = Items[i].GetFormattedValue("FutureState");
                                            break;
                                        case "Plans":
                                            table0[1, 3].TextFrame.Text = Items[i].GetFormattedValue("PresentState");
                                            table0[2, 3].TextFrame.Text = Items[i].GetFormattedValue("TransitionState");
                                            table0[3, 3].TextFrame.Text = Items[i].GetFormattedValue("FutureState");
                                            break;
                                        case "Purse":
                                            table0[1, 4].TextFrame.Text = Items[i].GetFormattedValue("PresentState");
                                            table0[2, 4].TextFrame.Text = Items[i].GetFormattedValue("TransitionState");
                                            table0[3, 4].TextFrame.Text = Items[i].GetFormattedValue("FutureState");
                                            break;
                                        case "Systems":
                                            table0[1, 5].TextFrame.Text = Items[i].GetFormattedValue("PresentState");
                                            table0[2, 5].TextFrame.Text = Items[i].GetFormattedValue("TransitionState");
                                            table0[3, 5].TextFrame.Text = Items[i].GetFormattedValue("FutureState");
                                            break;
                                    }
                                }
                            }
                        }

                        System.Threading.Thread.Sleep(300);
                        MemoryStream MS0 = new MemoryStream();

                        PPTX0.SaveToFile(MS0, Spire.Presentation.FileFormat.Pptx2013);

                        List.RootFolder.Files.Add(PlanningFile.ServerRelativeUrl, MS0.ToArray(), true);
                    }
                    Site.Dispose();
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region Create Document
        internal int CreateDocument(string FileName, string ContentType, string ListName)
        {
            
            try
            {
                SPSite Site = SPContext.Current.Site;
                //SPSite site = new SPSite("http://csepp187/closeout/closeout/");
                using (SPWeb Web = Site.OpenWeb())
                {
                    SPList List = Web.Lists[ListName];
                    //First let's see if it's already in there...
                    SPQuery Query = new SPQuery();
                    //Define the Query...
                    Query.Query = @"<Where>
                                      <Eq>
                                        <FieldRef Name='LinkFilename'></FieldRef>
                                        <Value Type='Text'>" + FileName + @"</Value>
                                      </Eq>
                                    </Where>";
                    //Set the ViewFields...
                    Query.ViewFields = @"<FieldRef Name='ID' />
                                    <FieldRef Name='Title' />
                                    <FieldRef Name='ContentType' />
                                    <FieldRef Name='Rating' />
                                    <FieldRef Name='Jurisdiction' />
                                    <FieldRef Name='FileLeafRef' />";
                    //Get the Items...
                    SPListItemCollection Items = List.GetItems(Query);
                    //If nothing comes back, this will be 0, so no loop...
                    foreach (SPListItem Item in Items)
                    {
                        return Int32.Parse(Item[SPBuiltInFieldId.ID].ToString());
                    }

                    // this always uses root folder
                    SPFolder Folder = List.RootFolder;//web.Folders[sList];
                    SPFileCollection FileCollection = Folder.Files;

                    // find the template url and open
                    string Template = List.ContentTypes[ContentType].DocumentTemplateUrl;
                    SPFile File = Web.GetFile(Template);
                    byte[] FileBytes = File.OpenBinary();
                    // Url for file to be created
                    string DestFile = FileCollection.Folder.Url + "/" + FileName;

                    // create the document and get SPFile/SPItem for 
                    // new document
                    SPFile NewFile = FileCollection.Add(DestFile, FileBytes, false);
                    SPItem NewItem = NewFile.Item;
                    NewItem["ContentType"] = ContentType;
                    NewItem.Update();
                    NewFile.Update();
                    return NewItem.ID;
                }
                
            }
            catch (SPException spEx)
            {
                // file already exists?
                if (spEx.ErrorCode == -2130575257)
                    return -1;
                else
                    throw spEx;
            }
            finally
            {
                //site.Dispose();
            }
        }
        #endregion

        #region Get Rating
        private String GetRating(System.Collections.ArrayList List)
        {
            int gc = 0, ac = 0, rc = 0;
            //We assign the lowest Rating to the final file...
            for (int i = 0; i < List.Count; i++)
            {
                SPListItem Item = (SPListItem)List[i];
                String Rating = Item.GetFormattedValue("Rating");

                switch (Rating)
                {
                    case "Red": rc++; break;
                    case "Amber": ac++; break;
                    case "Green": gc++; break;
                }
            }

            if (rc > 0) { return "Red"; }
            if (ac > 0) { return "Amber"; }
            if (gc > 0) { return "Green"; }

            return "Red";
        }
        #endregion
    }
}
