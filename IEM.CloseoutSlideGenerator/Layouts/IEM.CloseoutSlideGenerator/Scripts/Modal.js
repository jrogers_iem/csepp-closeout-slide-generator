﻿
 $(document).ready(function(){
     //In SharePoint OOTB list view, all 'td' will have this class 'ms-cellstyle'
     $("td.ms-cellstyle a").click(function () {
     //$("td.ms-vb2 a").click(function () {
         var currentURL = $(this).attr('href'); 
         var onclickVal = $(this).attr('onclick') || '';
         if(onclickVal == '') {
             currentURL = "javascript:ModalDialog('"+currentURL+"')";           
             $(this).attr('onclick', 'empty');     
             $(this).attr('href', currentURL);        
         }
     });
 });
   

 function StatusDialog(urlvalue) {
     var options = {
         url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue,
         autoSize: true,
         allowMaximize: false,
         showClose: true,
         dialogReturnValueCallback: refreshCallback
     };
     SP.UI.ModalDialog.showModalDialog(options);
 }

 function ModalDialog(urlvalue) {     
     
    var options = {
        url: urlvalue,
        width: 900,        
        allowMaximize: true,
        showClose: true,     
        dialogReturnValueCallback: silentCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}
function silentCallback(dialogResult, returnValue) {
}
function refreshCallback(dialogResult, returnValue) {
    SP.UI.Notify.addNotification('Operation Complete!');
    SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
}

function Section1(urlvalue, source) {
        
    var currentFolder = getUrlParameter('RootFolder');  

    var options = {
        url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue +'&Folder=' + currentFolder,
        autoSize:true,
        allowMaximize: false,
        showClose: true,
        width: 900,
        dialogReturnValueCallback: refreshCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}
function Section2(urlvalue, source) {

    if (confirm('Are you sure you want to send this item to the Recycle Bin?')) {
        var currentFolder = getUrlParameter('RootFolder');

        var options = {
            url: _spPageContextInfo.webAbsoluteUrl + '/' + urlvalue + '&Folder=' + currentFolder,
            autoSize: true,
            allowMaximize: false,
            showClose: true,
            dialogReturnValueCallback: refreshCallback
        };
        SP.UI.ModalDialog.showModalDialog(options);
    }
}
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};



