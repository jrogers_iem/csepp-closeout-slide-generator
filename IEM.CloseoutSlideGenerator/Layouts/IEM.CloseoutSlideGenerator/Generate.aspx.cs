﻿using System;

using System.Data;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

using Spire.Presentation;
using IEM.CloseoutSlideGenerator.Util;

namespace IEM.CloseoutSlideGenerator.Layouts.IEM.CloseoutSlideGenerator
{
    public partial class Generate : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Label1.Text = Request.QueryString["ListId"];
                //Label2.Text = Request.QueryString["SelectedItemId"];
                //Label3.Text = Request.QueryString["SiteURL"];

                BindControl();
            }
            catch (Exception ex)
            {
                //Label1.Text = ex.Message;
            }
        }

        //public SPUser GetSPUser(SPListItem item, string key)
        //{
        //    SPFieldUser field = item.Fields[key] as SPFieldUser;

        //    if (field != null)
        //    {
        //        SPFieldUserValue fieldValue = field.GetFieldValue(item[key].ToString()) as SPFieldUserValue;

        //        if (fieldValue != null)
        //            return fieldValue.User;
        //    }
        //    return null;
        //}

        internal void BindControl()
        {
            DropDownList1.Items.Clear();
            SPListCollection Lists = SPContext.Current.Web.Lists;

            foreach (SPList L in Lists)
            {
                if (L.BaseType == SPBaseType.DocumentLibrary)
                {
                    DropDownList1.Items.Add(L.Title);
                }
            }

            SPList List = SPContext.Current.Web.Lists.GetList(new Guid(Request.QueryString["ListId"]), true);

            String[] IDs = Request.QueryString["SelectedItemId"].Split(new string[] { "," }, StringSplitOptions.None);

            DataTable T = GetTable();

            for (int i = 0; i < IDs.Length; i++)
            {
                SPListItem Item = List.GetItemByIdAllFields(Int32.Parse(IDs[i]));

                DataRow R = T.NewRow();

                R["Version"] = Item.GetFormattedValue(Item.Fields[SPBuiltInFieldId._UIVersionString].Title);
                R["Title"] = Item.GetFormattedValue(Item.Fields[SPBuiltInFieldId.Title].Title);

                string currentValue = Item["Editor"].ToString();
                SPFieldUser userField = (SPFieldUser)Item.Fields[SPBuiltInFieldId.Editor];
                SPFieldUserValue itemValue = (SPFieldUserValue)userField.GetFieldValue(currentValue);
                SPUser user = itemValue.User;                

                R["Modified By"] = user.Name; 

                String Rating = Item.GetFormattedValue("Rating");

                if (Rating == "Red") { R["KPI"] = "/_layouts/images/kpidefault-2.gif"; }
                if (Rating == "Amber") { R["KPI"] = "/_layouts/images/kpidefault-1.gif"; }
                if (Rating == "Green") { R["KPI"] = "/_layouts/images/kpidefault-0.gif"; }

                R["Due Date"] = Item.GetFormattedValue(Item.Fields[SPBuiltInFieldId.TaskDueDate].Title);
                R["Task Status"] = Item.GetFormattedValue(Item.Fields[SPBuiltInFieldId.TaskStatus].Title);
                R["Modified"] = Item.GetFormattedValue(Item.Fields[SPBuiltInFieldId.Modified].Title);
                                
                string currentValue1 = Item["Assigned To"].ToString();
                SPFieldUser userField1 = (SPFieldUser)Item.Fields[SPBuiltInFieldId.AssignedTo];
                SPFieldUserValue itemValue1 = (SPFieldUserValue)userField.GetFieldValue(currentValue1);
                SPUser user1 = itemValue.User;

                R["Assigned To"] = user1.Name;

                T.Rows.Add(R);
            }

            GridView1.DataSource = T;
            GridView1.DataBind();

        }

        internal DataTable GetTable()
        {
            DataTable T = new DataTable();

            T.Columns.Add("Version");
            T.Columns.Add("Title");
            T.Columns.Add("Modified");
            T.Columns.Add("Modified By");
            T.Columns.Add("KPI");
            T.Columns.Add("Assigned To");
            T.Columns.Add("Due Date");
            T.Columns.Add("Task Status");

            return T;
        }

        /// <summary>
        /// Generate Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (SPLongOperation OP = new SPLongOperation(this.Page))
                {
                    OP.LeadingHTML = "Converting Closeout Tasks to PowerPoint Slides ....";
                    OP.TrailingHTML = "Please wait ....";

                    OP.Begin();

                    String[] Param = Request.QueryString["SelectedItemId"].Split(new char[] { ',' });
                    //
                    int[] IDs = Array.ConvertAll(Param, int.Parse);

                    SlideUtil Util = new SlideUtil();

                    Util.CreateSlides(IDs, DropDownList1.SelectedItem.Text);


                    // check if the current page is opened in dialog
                    if (SPContext.Current.IsPopUI)
                        // signal that the lengthy operation is completed and 
                        // send a script to close the current dialog
                        OP.EndScript("window.frameElement.commitPopup();");
                    else
                        // signal that the lengthy operation is completed and 
                        // redirect to another page
                        OP.End("/_layouts/15/start.aspx#/");
                }


               // SPLongOperation OP = new SPLongOperation(this.Page);
               // OP.LeadingHTML = "Converting Closeout Tasks to PowerPoint Slides....";
               // OP.TrailingHTML = "Please Wait...";
               //// OP.EndScript("<script type='text/javascript'>window.frameElement.commitPopup();</script>");
               // OP.Begin();
               // //
               // String[] Param = Request.QueryString["SelectedItemId"].Split(new char[] { ',' });
               // //
               // int[] IDs = Array.ConvertAll(Param, int.Parse);

               // SlideUtil Util = new SlideUtil();

               // Util.CreateSlides(IDs, DropDownList1.SelectedItem.Text);

               // OP.End("");


                //Response.Write("<script type='text/javascript'>window.frameElement.commitPopup();</script>");
                //Response.Flush();
                //Response.End();
            }
            catch (Exception ex)
            {
                Label1.Text = ex.Message;
            }
        }
        /// <summary>
        /// Cancel Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {

        }

        // Creates document in given list (root folder). 
        // Returns true if the file was created, false if it already
        // exists or throws exception for other failure
        //protected bool CreateDocument(string sFilename, string sContentType, string sList)
        //{
        //    try
        //    {
        //        SPSite site = SPContext.Current.Site;

        //        using (SPWeb web = site.OpenWeb())
        //        {
        //            SPList list = web.Lists[sList];
        //            // this always uses root folder
        //            SPFolder folder = list.RootFolder;//web.Folders[sList];
        //            SPFileCollection fcol = folder.Files;

        //            // find the template url and open
        //            string sTemplate = list.ContentTypes[sContentType].DocumentTemplateUrl;
        //            SPFile spf = web.GetFile(sTemplate);
        //            byte[] binFile = spf.OpenBinary();
        //            // Url for file to be created
        //            string destFile = fcol.Folder.Url + "/" + sFilename;

        //            // create the document and get SPFile/SPItem for 
        //            // new document
        //            SPFile addedFile = fcol.Add(destFile, binFile, false);
        //            SPItem newItem = addedFile.Item;
        //            newItem["ContentType"] = sContentType;
        //            newItem.Update();
        //            addedFile.Update();
        //            return true;
        //        }
        //    }
        //    catch (SPException spEx)
        //    {
        //        // file already exists?
        //        if (spEx.ErrorCode == -2130575257)
        //            return false;
        //        else
        //            throw spEx;
        //    }
        //}

    }
}
